## What did you say?

The app should:

1. Display a page with controls to allow the user to enter a word/phrase
2. A View button to view the data
3. When the view button is pressed the app should connect to the Facebook Graph API and display a list of public posts with location data where the text contains the specified word/phrase within the period specified
4. Bonus points for doing something interesting with the data e.g. maps display, charting, sentiment analysis

Alternatively perform the same actions against the Twitter search API.

The README file with the code should explain how it works and any instructions needed to make it work (e.g. API credentials, etc)

### Configuration

In order to use the Facebook Graph API, you need an access token, which you can get through Facebook's [Graph API Explorer](https://developers.facebook.com/tools/explorer) (click on 'Get Access Token'). Then you must set your token editing the `config/facebook.yml` file. Be sure to allow `read_streams` to get location data.

### Where to go next

Based on the "*Don’t spend more than 1.25 hours on this app.*" advice, the current code was the first working approach that delivers what was requested under 75 mins. Including the time reading the FB's API.

While it does the job, there are room for improvement. Some of them are listed below:

* In the form, there is no label for the start date and end date. Not a very good user experience, huh?
* If the dates are invalid, it would be better to display errors for the user instead of just ignoring it.
* The since date cannot be set	 before 1/1/1970. Again, this could be fixed with a proper date validation.
* While reading posts, some returns without a message attribute, it might be worth spending some time on it to remove the `return if post["message"].blank?` inside the `_post.html.haml` partial.
* We could improve the logic to show the results inside the `index.html.haml`.
* Write tests.

### One more thing...
I could have spent all the weekend working on the app, but it would feel like cheating.

This project shows a little of my coding style.

I don't like giving too much responsability to controllers. I avoid using nested forms, I'm refactoring the ones I have with form objects instead and I am liking it a lot. [This gist](https://gist.github.com/shamanime/1d72dbe504675aa91e6f) is an example that I wrote on the past week. I made the gist and contacted Reform's author and he was kind enough to review and say the code was OK.

Well, thanks for the ride!