class WordSearch
  def initialize(query, start_date, end_date)
    @query = query
    sanitize_dates(start_date, end_date)
  end

  def results
    @posts ||= find_posts
  end

  private
    def find_posts
      return nil if @query.blank?

      rest = Koala::Facebook::API.new
      results = rest.search(@query, type: 'post', fields: 'id,from,message,place', 'since' => @start_date, 'until' => @end_date)
      results
    end

    def sanitize_dates(start_date, end_date)
      @start_date = Date.parse(start_date).to_s rescue nil
      @end_date   = (Date.parse(end_date) + 1.day).to_s rescue nil
    end
end
