class SearchController < ApplicationController
  def index
    if params[:q] && params[:since] && params[:until]
      @results = WordSearch.new(params[:q], params[:since], params[:until]).results
    end
  end
end
