# config/initializers/koala.rb
# Monkey-patch in Facebook config so Koala knows to
# automatically use Facebook settings from here if none are given

module Facebook
  CONFIG = YAML.load_file(Rails.root.join("config/facebook.yml"))[Rails.env]
  ACCESS_TOKEN = CONFIG['access_token']
end

Koala::Facebook::API.class_eval do
  def initialize_with_default_settings(*args)
    case args.size
      when 0, 1
        raise "access token is not specified in the config" unless Facebook::ACCESS_TOKEN
        initialize_without_default_settings(Facebook::ACCESS_TOKEN.to_s, args.first)
      when 2
        initialize_without_default_settings(*args)
    end
  end

  alias_method_chain :initialize, :default_settings
end

